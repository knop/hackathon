var gulp = require('gulp');
var mocha = require('gulp-mocha');
var mutationTesting = require('gulp-mutation-testing');
var istanbul = require('gulp-istanbul');


gulp.task('pre-test', function () {
  return gulp.src(['src/*.js'])
    // Covering files
    .pipe(istanbul())
    // Force `require` to return covered files
    .pipe(istanbul.hookRequire());
});

gulp.task('test',['pre-test'], function () {
  return gulp.src('test/**/*.js')
  .pipe(mocha({reporter: 'nyan'}))
  // Creating the reports after tests ran
  .pipe(istanbul.writeReports())
  // Enforce a coverage of at least 90%
  .pipe(istanbul.enforceThresholds({ thresholds: { global: 100 } }));

  ;
});

gulp.task('mutate', function () {
  return gulp.src('test/controller.spec.js')
    .pipe(mutationTesting('src/controller.js'));
});
