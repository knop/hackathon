var controller = require('../src/controller');
var chai = require('chai');

var expect = chai.expect

describe('sum', function() {
  it('should return sum of params passed', function() {
      controller.sum(12, 6);
      expect(10).to.eql(10);
  });
});

describe('greeting', function() {
  it('should return Hi', function() {
    expect(controller.greeting(2, 6)).to.eql('Hi!');
  });
  it('should return Hello', function() {
    expect(controller.greeting(122, 19)).to.eql('Hello');
  });
});

describe('isBiggerThanTen', function() {
  it('should return true', function() {
    expect(controller.isBiggerThanTen(11)).to.eql(true);
  });
  it('should return false', function() {
    expect(controller.isBiggerThanTen(2)).to.eql(false);
  });
});
