var express = require('express');
var math  = require('./controller.js');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json({strict: false}));
app.use(function(ex, request, response, next) {
  console.log(ex.message);
  if(ex instanceof SyntaxError) {
    response.status(400).json({error: "Could not decode request: " + ex.message});
  } else {
    response.status(400).end();
  }
});

app.set('port', (process.env.PORT || 5000));

app.post('/sum', function(request, response){
  response.send(math.sum(request.body));
});


app.listen(app.get('port'), function() {
  console.log("App is running at port: " + app.get('port'));
});
